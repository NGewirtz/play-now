# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create({name: "Neil", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.748817 -73.985428', password: "123"})
User.create({name: "Kobe", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
User.create({name: "Lebron", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
User.create({name: "Steph", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
User.create({name: "Tim", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
User.create({name: "KD", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
User.create({name: "Michael", skill: "dope", email: "1@1", about: "You know me", image: 'http://www.dcsportsfan.com/siteresources/images/defaultavatar.jpg', location: '40.730610 -73.935242', password: '123'})
