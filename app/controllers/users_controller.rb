class UsersController < ApplicationController

	def index
		@user = User.new
    if logged_in?
      redirect_to user_path(session[:user_id])
    else
      redirect_to "/"
    end
	end

	def login
			user = User.find_by({name: params[:name]})
		 if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to user_path(user)
    else
      redirect_to "/"
    end
	end


	def new
    @user = User.new
  end

  def create
    @new_user = User.create(user_params)
    if @new_user.save
    	session[:user_id] = @new_user.id
    	redirect_to user_path(@new_user)
    else
    	redirect_to ("/users/new")
    end
  end

  def show
    if logged_in?
      actual_user = User.find(session[:user_id])
      if logged_in? && check_current_user?
        @user = User.find(session[:user_id])
      else
        redirect_to user_path(actual_user)
      end
    # if logged_in?
    #   @user = User.find(session[:user_id])
    else
      redirect_to ("/users/new")
    end
  end


  def image
  	@user = User.find(params[:id])
  	if params["image"]
  		@user.update(image: params["image"])
 	 	end
 	 	if params["skill"]
 	 		@user.update(skill: params["skill"])
 	 	end
 	 	if params["about"]
  		@user.update(about: params["about"])
  	end
  	if params["location"]
  		@user.update(location: params["location"])
  		redirect_to '/games'
  	else
			redirect_to user_path(@user)
		end
  end

  def readonly
    @user = User.find(params[:id])
  end



  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :location, :image, :authenticity_token, :skill)
  end



end