class GamesController < ApplicationController

	def index
		if logged_in? && User.find(session[:user_id])[:location] != ""
			@userid = session[:user_id]
			@currentuser = User.find(@userid)
		else
			redirect_to "/users/5"
		end
	end

	def haversine_distance
		@userid = session[:user_id]
		@all = []
		@user = User.all
		@RAD_PER_DEG = 0.017453293
		test = User.find(@userid)
		x = test.location
		x = x.split(" ")
		lat1 = x[0].to_f
		lon1 = x[1].to_f
		@Rmiles = 3956  
		@Rkm = 6371
		@Rfeet = @Rmiles * 5282
		@Rmeters = @Rkm * 1000
		@user.each do |u|
			if u[:location]
				@distances = {}


				@distances["id"] = u[:id]
				@distances["online"] = u[:online]
				@distances["name"] = u[:name]
				u = u.location
				u = u.split(" ")


				lat2 = u[0].to_f
				lon2 = u[1].to_f


		  
			  dlon = lon2 - lon1  
			  dlat = lat2 - lat1  
			   
			  dlon_rad = dlon * @RAD_PER_DEG  
			  dlat_rad = dlat * @RAD_PER_DEG  
			   
			  lat1_rad = lat1 * @RAD_PER_DEG  
			  lon1_rad = lon1 * @RAD_PER_DEG  
			   
			  lat2_rad = lat2 * @RAD_PER_DEG  
			  lon2_rad = lon2 * @RAD_PER_DEG  
			   
			  # puts "dlon: #{dlon}, dlon_rad: #{dlon_rad}, dlat: #{dlat}, dlat_rad: #{dlat_rad}"  
			   
			  a = Math.sin(dlat_rad/2)**2 + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(dlon_rad/2)**2  
			  c = 2 * Math.asin( Math.sqrt(a))  
			   
			  dMi = @Rmiles * c          # delta between the two points in miles  
			  dKm = @Rkm * c             # delta in kilometers  
			  dFeet = @Rfeet * c         # delta in feet  
			  dMeters = @Rmeters * c     # delta in meters  
			   
			  @distances["mi"] = dMi  
			  @distances["km"] = dKm  
			  @distances["ft"] = dFeet  
			  @distances["m"] = dMeters  


			  @all.push(@distances)
			end

		end
		@userid = session[:user_id]
		render :index
	end  

	def create
		render :index
	end

	def show
		if logged_in?
			if User.find(session[:user_id])[:location] == ""
				redirect_to "/users/5"
			end
		else
			redirect_to "/"
		end
	end

end